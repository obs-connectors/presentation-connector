﻿namespace presentation_connector
{
    public interface IPresentationService
    {
        public bool HideAll();
        public bool ShowType(ItemType itemType);
    }
}
