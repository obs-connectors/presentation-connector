﻿using System;

namespace presentation_connector
{
    public interface IStreamService
    {
        public Exception LastException { get; }

        public event PollEventHandler OnPollUpdate;
        public void Listen();
        public void Ignore();
        public ItemType GetItemType(string itemID);
    }
}
