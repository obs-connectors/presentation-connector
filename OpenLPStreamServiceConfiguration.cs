﻿using System.Xml.Serialization;

namespace presentation_connector
{
    [XmlRoot(ElementName = "OpenLPStreamServiceConfiguration")]
    public class OpenLPStreamServiceConfiguration
    {
        public static OpenLPStreamServiceConfiguration GetDummy()
            => new()
            {
                Address = "ip:port",
                Credentials = "credentials"
            };

        [XmlElement(ElementName = "Address")]
        public string Address { get; set; }

        [XmlElement(ElementName = "Credentials")]
        public string Credentials { get; set; }
    }
}
