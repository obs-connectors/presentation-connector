﻿using OBSWebsocketDotNet;
using System;
using System.Diagnostics;
using System.Linq;
using System.Timers;

namespace presentation_connector
{
    public class OBSPresentationService : IPresentationService
    {
        private readonly OBSPresentationServiceConfiguration configuration;
        private readonly OBSWebsocket obsWebsocket = new();
        private readonly Timer obsChecker = new(1000);

        public OBSPresentationService(OBSPresentationServiceConfiguration configuration)
        {
            this.configuration = configuration;

            obsWebsocket.Disconnected += (object sender, EventArgs e) => obsChecker.Start();

            obsChecker.Elapsed += (object checkerTimer, ElapsedEventArgs e) =>
            {
                if (Process.GetProcessesByName("obs64").Length > 0)
                {
                    obsChecker.Stop();
                    try
                    {
                        obsWebsocket.Connect(configuration.Address, configuration.Credentials);
                    }
                    catch (Exception)
                    {
                        obsChecker.Start();
                        return;
                    }
                    HideAll();
                }
            };
            obsChecker.Start();
        }

        public bool HideAll()
        {
            if (obsWebsocket.IsConnected) return configuration.ItemTypeMappings
                .FindAll(mapping => mapping.ItemType != ItemType.UNKNOWN)
                .SelectMany(mapping => mapping.Targets)
                .Select(target => SetVisibility(false, target.SourceName, target.SceneName))
                .All(i => i);
            else return false;
        }

        public bool ShowType(ItemType itemType)
        {
            if (obsWebsocket.IsConnected)
            {
                HideAll();

                var mapping = configuration.ItemTypeMappings.Find(mapping => mapping.ItemType != ItemType.UNKNOWN && mapping.ItemType == itemType);
                
                if (mapping != null) return mapping.Targets
                    .Select(target => SetVisibility(true, target.SourceName, target.SceneName))
                    .All(i => i);
                
                else Console.WriteLine($"No mapping found for item type {itemType}.");
                return true;
            }
            return false;
        }

        private bool SetVisibility(bool visible, string sourceName, string sceneName)
        {
            try
            {
                obsWebsocket.SetSourceRender(sourceName, visible, sceneName);
                if (obsWebsocket.GetSceneItemProperties(sourceName, sceneName).Visible == visible) return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error showing (source, scene) item ({sourceName}, {sceneName}): " + e.Message);
            }
            return false;
        }
    }
}
