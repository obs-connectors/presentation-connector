using System;
using System.Windows.Forms;

namespace presentation_connector
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            OpenLPStreamServiceConfiguration openLPStreamServiceConfiguration = XMLUtils<OpenLPStreamServiceConfiguration>.FromXML("openlp-config.xml");
            OBSPresentationServiceConfiguration obsPresentationServiceConfiguration = XMLUtils<OBSPresentationServiceConfiguration>.FromXML("obs-config.xml");
            if (openLPStreamServiceConfiguration != null && obsPresentationServiceConfiguration != null)
            {
                PresentationConnector presentationConnector = new(
                    new OpenLPStreamService(openLPStreamServiceConfiguration),
                    new OBSPresentationService(obsPresentationServiceConfiguration));
                presentationConnector.FormClosing += OnFormClosing;

                Application.Run(presentationConnector);
            }
            else
            {
                bool? initSuccessful = null;
                if (openLPStreamServiceConfiguration == null)
                {
                    if (InitEmptyConfigurationFile("OpenLP"))
                        initSuccessful = XMLUtils<OpenLPStreamServiceConfiguration>.InitXML("openlp-config.xml", OpenLPStreamServiceConfiguration.GetDummy());
                }
                else if (obsPresentationServiceConfiguration == null)
                {
                    if (InitEmptyConfigurationFile("OBS"))
                        initSuccessful = XMLUtils<OBSPresentationServiceConfiguration>.InitXML("obs-config.xml", OBSPresentationServiceConfiguration.GetDummy());
                }

                if (initSuccessful == true)
                    MessageBox.Show("Successfully initialized configuration file. Please adapt the file and restart the program.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (initSuccessful == false)
                    MessageBox.Show("Error initializing configuration file. Please investigate manually.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Do you want to close the Software?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) 
                e.Cancel = true;
        }

        private static bool InitEmptyConfigurationFile(string name)
            => MessageBox.Show(
                $"{name} configuration file is missing in the app directory. The program can not be loaded. Initialize empty configuration file now?",
                "Missing Configuration File",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Error) == DialogResult.Yes;
    }
}
