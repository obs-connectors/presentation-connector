﻿using System.Diagnostics;
using System.Windows.Forms;

namespace presentation_connector
{
    public partial class PresentationConnector : Form
    {
        private IStreamService streamService;
        public PresentationConnector(IStreamService streamService, IPresentationService presentationService)
        {
            this.streamService = streamService;
            InitializeComponent();
            SetUIBindings();

            streamService.OnPollUpdate += (PollMessage pollMessage) => BeginInvoke((MethodInvoker)delegate
            {
                Debug.WriteLine("Received PollMessage to handle.");
                lastStreamServiceSignal = System.DateTime.Now;
                lastBlankView = pollMessage.BlankView;

                if (!pollMessage.CurrentItemID.Equals(string.Empty) && !pollMessage.BlankView)
                {
                    ItemType lastItemType = streamService.GetItemType(pollMessage.CurrentItemID);
                    lastPresentationServiceActionSuccessful = presentationService.ShowType(lastItemType);
                    this.lastItemType = lastItemType;
                }
                else
                    lastPresentationServiceActionSuccessful = presentationService.HideAll();

                UpdateUI();
            });
            streamService.Listen();
        }
    }
}
